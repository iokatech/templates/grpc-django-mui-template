"""
WSGI config for django project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/howto/deployment/wsgi/
"""

import os
import sys

sys.path.append(os.getcwd() + '/api')

from django.core.wsgi import get_wsgi_application
from sonora.wsgi import grpcWSGI
from api.users.v1.users_pb2_grpc import add_UserServiceServicer_to_server
from apps.users.users_service import UserService

class customGrpcWSGI(grpcWSGI):
    """Fixup CORS handling.

       Library incorrectly uses the HTTP_HOST as the primary way to set the
       allow origin header.  This doesn't work when using CORS because the host
       will likely be different from the origin so the allow origin header will
       only have the destination host - where it really should use the origin
       as it's value.

    Args:
        grpcWSGI (_type_): _description_
    """    
    def _patch_environ(self, environ):
        new_environ = {key: value for key, value in environ.items()}
        new_environ['HTTP_HOST'] = environ['HTTP_ORIGIN']        
        return new_environ

    def _do_grpc_request(self, rpc_method, environ, start_response):
        accept_type = environ.get("HTTP_ACCEPT", "*/*")
        if accept_type == "*/*":
            del environ["HTTP_ACCEPT"]
        return super()._do_grpc_request(rpc_method, self._patch_environ(environ), start_response) # type: ignore

    def _do_cors_preflight(self, environ, start_response):
        return super()._do_cors_preflight(self._patch_environ(environ), start_response) # type: ignore

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')

application = get_wsgi_application()

# Install the Sonora grpcWSGI middleware so we can handle requests to gRPC's paths.

application = customGrpcWSGI(application)
add_UserServiceServicer_to_server(UserService(), application) # type: ignore

#application = CORS(application, headers="*", methods="*", maxage="120", origin="http://127.0.0.1:3000")