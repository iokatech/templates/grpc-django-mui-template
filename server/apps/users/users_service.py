from typing import Iterator
from queue import Queue, Empty
from functools import partial
from api.users.v1.users_pb2_grpc import UserServiceServicer
from api.users.v1.users_pb2 import (
    ListUsersResponse, User as PbUser, CreateUserRequest, 
    CreateUserResponse, DeleteUserRequest, DeleteUserResponse, 
    OnUserEventsResponse, USER_EVENT_TYPE_CREATED,
    USER_EVENT_TYPE_DELETED, UserEventType
)
from .models import User
from .signals import users_signal

class UserService(UserServiceServicer):
    def ListUsers(self, request, context) -> ListUsersResponse:
        users = User.objects.all()
        resp = ListUsersResponse()
        for user in users:
            resp.users.append(PbUser(
                    id=user.pk,
                    email=user.email,
                    username=user.username,
                    first_name=user.first_name,
                    last_name=user.last_name,
                ))        
        return resp

    def CreateUser(self, request:CreateUserRequest, context) -> CreateUserResponse:
        user: User = User.objects.create(
            username=request.user.username,
            first_name=request.user.first_name,
            last_name=request.user.last_name,
            email=request.user.email
        )
        ret = CreateUserResponse(user=PbUser(
            id=user.pk,
            username=user.username,
            first_name=user.first_name,
            last_name=user.last_name,
        ))
        users_signal.send(USER_EVENT_TYPE_CREATED)
        return ret

    def DeleteUser(self, request:DeleteUserRequest, context) -> DeleteUserResponse:
        User.objects.filter(id=request.id).delete()
        ret = DeleteUserResponse()
        users_signal.send(USER_EVENT_TYPE_DELETED)
        return ret

    def OnUserEvents(self, request, context) -> Iterator[OnUserEventsResponse]:
        queue: Queue[UserEventType] = Queue()

        def signal_listner(queue: Queue[OnUserEventsResponse], sender, **kwargs):
            queue.put(sender)

        def response_message() -> Iterator[OnUserEventsResponse]:
            while True:
                try:
                    sender = queue.get(True, 0.5)
                except Empty:
                    continue
                yield OnUserEventsResponse(
                    event_type=sender
                )

        users_signal.connect(partial(signal_listner, queue))

        return response_message()
