import { CssBaseline } from '@mui/material';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import HomePage from './pages/Home';

// Add to the following for all other 'pages"
const router = createBrowserRouter([
  {
    path: '/',
    element: <HomePage />,
  },
]);

export function App() {
  return (
    <>
      <CssBaseline />
      <RouterProvider router={router} />
    </>
  );
}
