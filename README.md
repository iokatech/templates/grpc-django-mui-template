# A minimal React grpc-django-mui-template example

To start a new project based off of this template, first install `degit` which is
a project scaffolding tool:

```bash
yarn global add tiged # This is a new forked and maintained version of degit that also supports
                      # git subgroups
```

Use `degit` to clone the template:

```bash
degit --subgroup https://gitlab.com/iokatech/templates/grpc-django-mui-template grpc-django
```

## Prequisites

* Requires node 18 (for global fetch), yarn, buf, and pipenv.
* Install dependencies with:

To install `buf` please refer to the following page: <https://docs.buf.build/installation>

```bash
pipenv install --dev
pipenv shell
yarn install # this also runs the prepare script which does the prisma generate and migrate
```

## Playing around

Make sure you have activated your pipenv

```bash
pipenv shell
```

```bash
yarn dev
```

Open multiple browser windows to [The App](http://localhost:3000) and enjoy!

Try editing the ts files to see the type checking in action :)

## Linting

```bash
yarn lint
yarn lint-fix
```

## Check for breaking changes

The API can be checked against the original to see if any changes have been
made that will cause breaking changes.

```bash
yarn breaking
```

## Building and Previewing Production

```bash
yarn build
yarn preview
```

## Developer Notes

To clear DB, production builds, and generated files:

```bash
yarn clean
```

## E2E testing

You maybe asked to install playwright globally with the following:

```bash
npx playwright install
```

To run E2E tests on the development environment:

```bash
yarn test-dev
```

To run E2E tests on the production build, you
first need to build the production environment with:

```bash
yarn build
```

Next, you can run tests with:

```bash
yarn test-preview
```
